# Base image

## s2i-python-container 
This Base image is probably been deployed in OpenShift. 

In order to investigate the flow of clb jupyter images deployment, we first pull the django-ex repo and then we build the python image with Dockerfile.  

Ref: [Python s2i container image](https://github.com/sclorg/s2i-python-container/tree/master/src)
